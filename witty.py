import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QTreeWidget, QTreeWidgetItem, QLineEdit, QWidget
from PyQt5.QtCore import QSize
from requests import get, put
from urllib.parse import quote

url = "http://192.168.178.16:9876/"

class WittyWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setWindowTitle("Witty Remote Control")

        self.tree = dict()
        self.playing = False

        self.play_pause_button = QtWidgets.QPushButton("Play")
        self.play_pause_button.clicked.connect(self.play_pause)

        self.stop_button = QtWidgets.QPushButton("Stop")
        self.stop_button.clicked.connect(self.stop)

        self.bluetooth_button = QtWidgets.QPushButton("Bluetooth")
        self.bluetooth_button.clicked.connect(self.bluetooth)

        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)
        gridLayout = QGridLayout()
        centralWidget.setLayout(gridLayout)

        self.filter = QLineEdit(self)
        self.filter.setPlaceholderText("Filter Media...")

        self.tree_widget = QTreeWidget(self)
        self.tree_widget.setHeaderHidden(True)
        self.tree_widget.itemClicked.connect(self.tree_clicked)

        gridLayout.addWidget(self.filter, 0, 0, 1, -1)
        gridLayout.addWidget(self.tree_widget, 1, 0, 1, -1)
        gridLayout.addWidget(self.play_pause_button, 2, 0)
        gridLayout.addWidget(self.stop_button, 2, 1)
        gridLayout.addWidget(self.bluetooth_button, 2, 2)

        self.update_tree()

    def play_pause(self):
        if (self.playing):
            self.playing = False
            self.play_pause_button.setText("Play")
            put(url + "player/pause")
        else:
            self.playing = True
            self.play_pause_button.setText("Pause")
            put(url + "player/play")

    def bluetooth(self):
        put(url + "player/connect_bluetooth")

    def stop(self):
        put(url + "player/stop")

    def update_tree(self):
        ret = get(url + "/")
        if (ret.status_code != 200):
            raise Exception('GET / {}'.format(ret.status_code))
        self.tree = ret.json()
        self.tree_widget.clear()
        for key, item in self.tree.items():
                widget = QTreeWidgetItem(self.tree_widget, [key])
                self.tree_widget.addTopLevelItem(widget)
                add_entries(widget, item, quote(key))

    def tree_clicked(self, widget, column):
        if hasattr(widget, 'url_path'):
            self.open(widget.url_path)

    def open(self, name):
        put(url + "player/open/{}".format(name))


def add_entries(widget, tree, path):
    if isinstance(tree, dict):
        if "path" in tree:
            widget.url_path = path
        else:
            for key, item in tree.items():
                subwidget = QTreeWidgetItem(widget, [key])
                add_entries(subwidget, item, path + "/" + quote(key))




if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWin = WittyWindow()
    mainWin.show()
    sys.exit( app.exec_() )
