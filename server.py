from typing import Optional
from fastapi import FastAPI
from subprocess import run, Popen, DEVNULL
from urllib.parse import unquote
from json import load
from os import environ

app = FastAPI()

# TODO
#
# -Update scraping
# -Handle error finding node more gracefully
# -Add API for editing json data
# -Add API command for scanning library
# -Add API command for getting state
# -Write terminal client
# -Write web client
# -Add support for looping through subtitles
# -On play, automatically mark as watched
# -make string in GET /<str:path>  optional 



# Helper Functions

def get_tree():
	fn = environ['MEDIA_HOME']+'/.media.json'
	data = None
	with open(fn) as json_file:
		data = load(json_file)
	return data

def get_node(path: str):
	path = path.split("/")
	node = get_tree()
	for i in path:
		if i != "":
			node = node[unquote(i)]
	return node


# Player Controls

@app.put("/player/stop")
def stop_media():
	run(['killall', 'vlc'])

@app.put("/player/open/{entry:path}")
def open_media(entry: str):
	stop_media()
	connect_bluetooth()
	Popen(['vlc', '--extraintf', 'http', environ['MEDIA_HOME']+get_node(entry)["path"]], stdout=DEVNULL, stderr=DEVNULL)
	# Needed to turn off terminal interface of vlc in order for it to respond to dbus volume change...

@app.put("/player/play")
def play_media():
	run(['playerctl', '-p', 'vlc', 'play'])

@app.put("/player/pause")
def pause_media():
	run(['playerctl', '-p', 'vlc', 'pause'])

@app.put("/player/seek/+{pos}")
def seek_media_forward(pos: float):
	run(['playerctl', '-p', 'vlc', 'position', str(pos)+'+'])

@app.put("/player/seek/-{pos}")
def seek_media_back(pos: float):
	run(['playerctl', '-p', 'vlc', 'position', str(pos)+'-'])

@app.put("/player/seek/{pos}")
def seek_media(pos: float):
	run(['playerctl', '-p', 'vlc', 'position', str(pos)])

@app.put("/player/volume/+{vol}")
def set_volume_up(vol: float):
	run(['playerctl', '-p', 'vlc', 'volume', str(vol)+'+'])

@app.put("/player/volume/-{vol}")
def set_volume_down(vol: float):
	run(['playerctl', '-p', 'vlc', 'volume', str(vol)+'-'])

@app.put("/player/volume/{vol}")
def set_volume_absolute(vol: float):
	run(['playerctl', '-p', 'vlc', 'volume', str(vol)])
	print(['playerctl', '-p', 'vlc', 'volume', str(vol)])
	run(['playerctl', '-l'])



# Miscellaneous Commands

@app.put("/command/connect_bluetooth")
def connect_bluetooth():
	run(['bluetoothctl', 'power', 'on'], stdout=DEVNULL, stderr=DEVNULL)
	Popen(['bluetoothctl', 'connect', 'B8:86:87:D8:16:CF'])



# Getting Media Tree

@app.get("/{entry:path}")
def get_media(entry:str):
	return get_node(entry)
