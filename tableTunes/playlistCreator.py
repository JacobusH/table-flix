import io
import json
import mimetypes
import os, subprocess
import uuid
import csv
from mutagen.easyid3 import EasyID3

class PlaylistCreator(object):
    allowed_mimetypes = ['csv']
    to_rip = '/InnerTome/music/to_rip.csv'
    f_exits = '/InnerTome/music/gottem'
    data_path_music = '/InnerTome/music/'
    # data_path_spotify_playlists = '/InnerTome/music/1 Playlists/'
    data_path_spotify_playlists = '/InnerTome/music/11 Playlists/'
    FNULL = open(os.devnull, 'w')

    def create_from_spotify_playlists(self):
        for root, dirs, files in os.walk(self.data_path_spotify_playlists):
            path = root.split(os.sep)
            for file in files: # go thru all files we have on disk
                mimetype = self.get_mimetype(file)
                cur_full_file = root + "/" + file
                cur_file_title = file.split('.')[-2]
                if mimetype in self.allowed_mimetypes:
                    with open(cur_full_file) as csv_file:
                        csv_reader = csv.reader(csv_file, delimiter=',')
                        line_count = 0
                        for row in csv_reader:
                            if line_count == 0:
                                print(f'Column names are {", ".join(row)}')
                                line_count += 1
                            else:
                                spot_uri = row[0]
                                title = row[1]
                                artist = row[2].split(',')[0]
                                if not self.exists(title, artist):
                                    # print(f'{row[1]} by {row[2]} does not exist on disk')
                                    with open(self.to_rip, 'a') as f:
                                        f.write(f'{spot_uri}, {title}, {artist}\n')
                                else:
                                    # print(f'{row[1]} by {row[2]} EXISTS')
                                    with open(self.f_exits, 'a') as f:
                                        f.write(f'{title}\n')
                                line_count += 1

    def exists(self, title, artist):
        for root, dirs, files in os.walk(os.path.join(self.data_path_music, artist)):
            for filename in files:
                # real_title = subprocess.check_output(['kid3-cli', '-c', 'get "title"', root+"/"+filename]).strip()
                # real_title = real_title.decode('utf-8')
                # real_title = filename.split('.')[0]
                if filename.split('.')[1] == 'mp3':
                    audio = EasyID3(root+"/"+filename)
                    real_title = audio['title'][0]

                    idx_paren_title = title.find(' (')
                    idx_paren_real_title = real_title.find(' (')
                    if idx_paren_title != -1:
                        title = title[:idx_paren_title]
                    if idx_paren_real_title != -1:
                        real_title = real_title[:idx_paren_real_title]

                    if title.lower() == real_title.lower():
                        return True
        return False
    
    def get_mimetype(self, filestr):
        return filestr.split('.')[-1]