MOVED TO SRCHUT

# Table Flix


## Debugging
> journalctl -xeu mediaserver.service

## SystemD unit file
> /etc/systemd/system/media-server.service

## Metadata storage location
> /storage/.media.json

## FastAPI server

Install python-fastapi uvicorn python-ujson playerctl, invoke as:

uvicorn server:app --reload --port 9876 --host 0.0.0.0

and set the MEDIA_HOME environment variable to your media root folder (/storage in our case).


