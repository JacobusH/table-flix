import io
import os
import json
import mimetypes
import requests
import uuid
import shutil
import re
from datetime import datetime
from enum import Enum
from os import environ

DataType = Enum('DataType', 'video music')

class Movie:
    def __init__(self, uuid, filename, title, genres, directors
    , year, rated, description, writers, actors, watched, subtitles):
        self.uuid       = uuid
        self.filename   = filename
        self.title      = title
        self.genres     = genres
        self.directors  = directors
        self.year       = year
        self.rated      = rated
        self.description = description
        self.writers    = writers
        self.actors     = actors
        self.watched    = watched
        self.subtitles  = subtitles

class Show:
    def __init__(self, uuid, filename, title, genres, directors
    , year, rated, description, writers, actors, watched, seasonNum, episodeNum):
        self.uuid       = uuid
        self.filename   = filename
        self.title      = title
        self.genres     = genres
        self.directors  = directors
        self.year       = year
        self.rated      = rated
        self.description = description
        self.writers    = writers
        self.actors     = actors
        self.watched    = watched
        self.seasonNum  = seasonNum
        self.episodeNum = episodeNum

class Song:
    def __init__(self, uuid, filename, title, genres, artist, albumArtist, trackNum, discNum, performer
    , nd, ft, vs, year, rated, listenCount):
        self.uuid           = uuid
        self.filename       = filename
        self.title          = title
        self.genres         = genres
        self.artist         = artist
        self.albumArtist    = albumArtist
        self.trackNum       = trackNum
        self.discNum        = discNum
        self.performer      = performer
        self.nd             = nd
        self.ft             = ft
        self.vs             = vs
        self.year           = year
        self.rated          = rated
        self.listenCount    = listenCount


class DataCollector:
    '''Scrapes hard drive for data'''

    base_path = environ['MEDIA_HOME']
    loc_videos = base_path + '/videos'
    loc_movies = loc_videos + '/movies'
    loc_shows = loc_videos + '/shows'

    loc_music = base_path + '/music'

    data_path_videos = '.media.json'
    data_path_music = '.music.json'

    data = {}
    allowed_mimetypes = ['mkv', 'mp4', 'avi', 'm4v']
    white_list = ['videos']

    def update_video_store(self, up_movies=True, up_shows=True, skip_meta=False):
        fn = os.path.join(self.base_path, self.data_path_videos)
        try: 
            with open(fn) as json_file:
                self.data = json.load(json_file)
        except: # create from skeleton if not exists
            shutil.copy(os.path.join(os.path.dirname(__file__), self.data_path_vid_skel), os.path.join(os.path.dirname(__file__), '../data/videos.json'))
            with open(fn) as json_file:
                self.data = json.load(json_file)

        # update all with data on hardrive and pull in metadata from the web
        if up_movies: self.scrape_movies() 
        if up_shows: self.scrape_shows(skip_meta)

    def update_music_store(self):
        self.scrape_music()

    def update_videos_new(self):
        tree = {}
        fn = os.path.join(os.path.dirname(__file__), self.data_path_videos)
        try: 
            with open(fn) as json_file:
                tree = json.load(json_file)
        except: # just have an empty {}
            pass

        self.update_videos_rec("/", tree)
        self.data = tree
        self.json_replace_all(DataType.video)

    def update_videos_rec(self, path, tree):
        if path.split('/')[1] not in self.white_list and path != '/':
            return 

        print(path)
        for item in os.listdir(self.base_path + path):
            full_path = self.base_path + path + item
            if os.path.isdir(full_path):
                tree[item] = {}
                self.update_videos_rec(path+item+'/', tree[item])
            if os.path.isfile(full_path):
                self.handle_file(path + item, tree)

    def handle_file(self, path, tree):
        is_lonely = False 
        mimetype = self.get_mimetype(self.base_path+path)
        dirpath = os.path.dirname(self.base_path+path)
        dirname = os.path.dirname(self.base_path+path).split('/')[-1]
        show_title = os.path.dirname(self.base_path+path).split('/')[-2]
        base_name = os.path.basename(self.base_path+path)
        title, ext = os.path.splitext(base_name) 

    
        if mimetype in self.allowed_mimetypes:
            if sum([(1 if (self.get_mimetype(sibling) in self.allowed_mimetypes) else 0) for sibling in os.listdir(dirpath)]) == 1: # is only child
                is_lonely = True
            season, episode = self.season_episode_parse(path)
            if season == "?": # movie
                v_api = VideoAPI()
                if is_lonely:
                    tree['title'] = dirname
                    # tree = v_api.get_video_by_title(dirname)
                else:
                    tree[title] = {'title': title}
                    # tree[title] = v_api.get_video_by_title(title)
            else: # show
                # new_show = Show(str(uuid.uuid4()), path, show_title, '', ''
                #     , '', '', '', ''
                #     , '', 0, season, episode)
                new_show = {'title': show_title, 'path': path, 'season': season, 'episode': episode, 'watched': 0}
                tree[title] = new_show

                # meta = v_api.get_video_by_title(title)
                # if meta["Response"] != "False": 
                #     new_show = Show(str(uuid.uuid4()), fullpath, title, meta["Genre"], meta["Director"]
                #         , meta["Year"], meta["imdbRating"], meta["Plot"], meta["Writer"]
                #         , meta["Actors"], 0, "Episode Number...")
                # else:


    def scrape_movies(self, refresh = False):
        # refresh = True: rescrapes metadata from internet and overwrites everything
        for root, dirs, files in os.walk(self.loc_movies):
            path = root.split(os.sep)
            for file in files: # go thru all files we have on disk
                mimetype = self.get_mimetype(file)
                cur_full_file = root + "/" + file
                cur_file_title = file.split('.')[0]
                if mimetype in self.allowed_mimetypes:
                    exists_movie = False
                    for movie in self.data["videos"]["movies"]:
                        if movie["title"] == cur_file_title:
                            if refresh: # overwrite existing metadata
                                self.movie_update(file, cur_full_file)
                            exists_movie = True
                            break
                    if not exists_movie: # fetch metadata and make entry in json
                        self.movie_add(file, cur_full_file)
        self.json_replace_all(DataType.video)

    def scrape_shows(self, skip_meta = False):
        for root, dirs, files in os.walk(self.loc_shows):
            path = root.split(os.sep)
            for file in files: # go thru all files we have on disk
                cur_full_file = root + "/" + file
                hier_list = path[3:] # remove first 3 entries from path ('', 'innertome', 'shows')
                self.show_add(file, cur_full_file, hier_list, skip_meta)
                # print(file)
        self.json_replace_all(DataType.video)

    def scrape_music(self):
        for root, dirs, files in os.walk(self.loc_music):
            path = root.split(os.sep)
            for file in files: # go thru all files we have on disk
                cur_full_file = root + "/" + file
                hier_list = path[3:] # remove first 3 entries from path ('', 'innertome', 'music')
                self.music_add(file, cur_full_file, hier_list)
                # print(file)
        self.json_replace_all(DataType.music)
       
    def show_add(self, title, fullpath, json_hier, skip_meta=False):
        num_season, num_episode = self.season_episode_parse(title)
        title = ' '.join(title.split(".")[:-1]) # remove extension
        v_api = VideoAPI()
        if not skip_meta:
            meta = v_api.get_video_by_title(title)
            if meta["Response"] != "False":
                new_show = Show(str(uuid.uuid4()), fullpath, title, meta["Genre"], meta["Director"]
                    , meta["Year"], meta["imdbRating"], meta["Plot"], meta["Writer"]
                    , meta["Actors"], 0, "Episode Number...")
                self.shows_tree_add(self.data["videos"]["shows"], json_hier, new_show.__dict__)
                # self.shows_map_add(self.data["videos"]["shows"], json_hier, new_show.__dict__)
            else:
                # TODO: make way to write errors to file
                print('{0} {1}'.format(title, meta["Error"]))
                if meta["Error"] == "Movie not found!": # still add entry minus metadata
                    new_show = Show(str(uuid.uuid4()), fullpath, title, None, None
                    , None, None, None, None
                    , None, 0, "Episode Number...")
                    self.shows_tree_add(self.data["videos"]["shows"], json_hier, new_show.__dict__)
                    # self.shows_map_add(self.data["videos"]["shows"], json_hier, new_show.__dict__)
        else: # just add without fetching metadata
            new_show = Show(str(uuid.uuid4()), fullpath, title, None, None
                    , None, None, None, None
                    , None, 0, num_season, num_episode)
            self.shows_tree_add(self.data["videos"]["shows"], json_hier, new_show.__dict__)
            # self.shows_map_add(self.data["videos"]["shows"], json_hier, new_show.__dict__)

    def music_add(self, filename, fullpath, json_hier):
        # TODO: make this
        
        title = ' '.join(filename.split(".")[:-1]) # remove extension

    def shows_tree_add(self, shows, hier_to_match, new_show):
        is_found = False
        if hier_to_match == []: # gotten all the way down correct branch to season list
            shows.append(new_show)
            return
        if not isinstance(shows, list):
            for k, v in shows.items():
                if isinstance(v, dict):
                    if k == hier_to_match[0]: # in correct branch
                        self.shows_tree_add(v, hier_to_match[1:], new_show)
                        return
                if k == hier_to_match[0]:
                    is_found = True
                    self.shows_tree_add(v, hier_to_match[1:], new_show)
            if not is_found: # add totally new show
                if len(hier_to_match) == 2: # show name, season
                    shows[hier_to_match[0]] = {hier_to_match[1]: [new_show]}
                if len(hier_to_match) == 1: # season
                    shows[hier_to_match[0]] = [new_show]

    def shows_map_add(self, shows, json_hier, new_show):
        pass
        # season = "Season {0}".format(new_show['seasonNum'])
        # episode = "Episode {0}".format(new_show['episodeNum'])
        # show_name = son_hier[0]

        # while len(json_hier) > 0 and json_hier[0] != "shows":
        #     json_hier.pop(0)
        # json_hier.pop(0) # get rid of "shows" too


        # if len(json_hier) == 2:
        #     if json_hier[0] not in self.data['videos']['shows']:
        #         self.data['videos']['shows'].update( {json_hier[0]: { json_hier[1]: { new_show['episodeNum']: new_show } } })
        #     else:
        #         self.data['videos']['shows'][json_hier[0]][json_hier[1]]new_show['episodeNum'] = new_show


        # if show_name not in self.data['videos']['shows']:

                

    def movie_add(self, title, fullpath):
        title = ' '.join(title.split(".")[:-1]) # remove extension
        v_api = VideoAPI()
        meta = v_api.get_video_by_title(title)
        if meta["Response"] != "False":
            new_movie = Movie(str(uuid.uuid4()), fullpath, title, meta["Genre"], meta["Director"]
                , meta["Year"], meta["imdbRating"], meta["Plot"], meta["Writer"]
                , meta["Actors"], 0, None)
            self.data["videos"]["movies"].append(new_movie.__dict__)
        else:
            # TODO: make way to write errors to file
            print('{0} {1}'.format(title, meta["Error"]))
            if meta["Error"] == "Movie not found!": # still add entry minus metadata
                new_movie = Movie(str(uuid.uuid4()), fullpath, title, None, None
                , None, None, None, None
                , None, 0, None)
                self.data["videos"]["movies"].append(new_movie.__dict__)

    def movie_update(self, title, fullpath):
        # title = title.split(".")[-2] # remove extension
        # v_api = VideoAPI()
        # meta = v_api.get_video_by_title(title)
        print('hi')
    
    ######
    ## HELPERS
    ######
    def get_mimetype(self, filestr):
        return filestr.split('.')[-1]

    def json_replace_all(self, which_dataType):
        self.rename_data_file(which_dataType) # rename old one
        fileDir = self.base_path
        if which_dataType == DataType.video:
            fn = os.path.join(fileDir, self.data_path_videos)
        elif which_dataType == DataType.music:
            fn = os.path.join(fileDir, self.data_path_music)
        with open(fn, 'w+') as outfile: # then make the new one
            json.dump(self.data, outfile)

    def rename_data_file(self, which_dataType):
        if which_dataType == DataType.video:
            fn = os.path.join(self.base_path, self.data_path_videos)
        elif which_dataType == DataType.music:
            fn = os.path.join(os.path.dirname(__file__), self.data_path_music)
        tmp = fn.replace('.json', '_{0}.json'.format(datetime.now().strftime('%Y-%m-%d_%H_%M')))
        os.rename(fn, tmp)

    def print_directory(self, direc):
        for root, dirs, files in os.walk(direc):
            path = root.split(os.sep)
            print((len(path) - 1) * '---', os.path.basename(root))
            for file in files:
                print(len(path) * '---', file)

    def season_episode_parse(self, title):
        # S01E09
        ret = None
        pattern = '[Ss](\d+)[Ee](\d+)'
        result = re.search(pattern, title) 
        if result and len(result.groups()) == 2:
            ret = result.groups()

        # 2x03
        if not ret:
            pattern = '(\d+)[Xx](\d+)'
            result = re.search(pattern, title) 
            if result and len(result.groups()) == 2:
                ret = result.groups()

            pattern = '[Ss]eason\s+(\d+)\s+[Ee]pisode\s+(\d+)'
            result = re.search(pattern, title) 
            if result and len(result.groups()) == 2:
                ret = result.groups()

            # TODO: figure this out...
            # 105.mp4
            # pattern = '(\d{3})\..[mkv|]$'
            # result = re.search(pattern, title) 
            # if not ret and result and len(result.groups()) == 1:
            #     if len(result.group(1)) == 3: # 105
            #         ret = (result.group(1)[:1], result.group(1)[1:])
            #     elif len(result.group(1)) == 4: # 1005
            #         ret = (result.group(1)[:2], result.group(1)[2:])
        
        if ret:
            return tuple([i.lstrip("0") for i in ret])

        return ('?', '?')

class VideoAPI:
    api_key = "7dc86faf"
    api_url_base = 'http://www.omdbapi.com/?apikey={0}&'.format(api_key)
    headers = {'Content-Type': 'application/json'}

    def get_video_by_title(self, title):
        title.replace(" ", "+")
        api_url = '{0}t={1}'.format(self.api_url_base, title)
        response = requests.get(api_url, headers=self.headers)

        if response.status_code == 200:
            return json.loads(response.content.decode('utf-8'))
        else:
            print("Fuck")
            return None

    def get_video_by_season_ep_tit(self, season, ep, title):
        title.replace(" ", "+")
        api_url = '{0}t={1}&Season={2}&Episode={3}'.format(self.api_url_base, title, season, ep)
        response = requests.get(api_url, headers=self.headers)

        if response.status_code == 200:
            return json.loads(response.content.decode('utf-8'))
        else:
            print("Fuck")
            return None


######
### MAIN
######
if __name__ == '__main__':
    dc = DataCollector()
    dc.update_videos_new()